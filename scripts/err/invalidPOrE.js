export default class InvalidEmailOrPassword extends Error {
    constructor(message) {
        super(message);
        this.name =
            "Error, Invalid email or Password";
    }
}
