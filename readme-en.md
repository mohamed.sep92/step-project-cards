## Exercise

You need to create a page where the Secretary can create cards describing scheduled doctor visits.

The page must contain:

1. Header (header) of the page:
    - in the upper left corner - the logo. You can take any
    - in the right corner - "Login" button. After successful authorization, it should change to the "Create visit" button.
2. Under Header - a form for filtering visits. This form should have 3 fields:
    - search by title/content of visit
    - search by status (Open/Done) (visit passed or not yet)
    - visit urgency (High, Normal, Low)
3. Under the filter form - a list of created visits.

#### Teamwork

In this project, all students are divided into groups of three people. Students can distribute tasks among themselves on their own. When submitting a project, it is necessary to indicate in the `Readme.md` file who performed which part of the task.

#### Technical requirements

  - The first time a user visits the page, the board should say `No items have been added`. The same inscription should be if the user does not have any added cards (for example, he deleted them all).
  - By clicking on the **Login** button, a modal window appears in which the user enters his email and password. If it is correct, the user is shown a list of previously created visits on the page.
  - By clicking on the **Create visit** button, a modal window appears in which you can create a new card.
  - To create classes, you need to use the `class` syntax from ES6.
  - You can use `fetch` or `axios` to make AJAX requests.
  - After performing any AJAX requests, the page should not be reloaded. When adding/removing a card and other similar operations, the entire list of cards **should not** be reloaded from the server. You need to use the data from the server response and Javascript to update the information on the page.
  - When you refresh the page or close it, previously added notes should not disappear.
  - It is advisable to divide the project into modules using ES6 modules. 

##### Modal window "Create visit"
   
The modal window should contain:

- Drop-down list (select) with the choice of doctor. Depending on the chosen doctor, under this drop-down list there will be fields that need to be filled in for a visit to this doctor.
- There should be three options in the drop-down list - **Cardiologist**, **Dentist**, **Therapist**.
- After selecting a doctor from the drop-down list, fields for recording to this doctor should appear under it. Several fields are the same for all three doctors:
   - purpose of the visit
   - brief description of the visit
   - drop-down field - urgency (normal, priority, urgent)
   - FULL NAME
- Also, each of the doctors has their own unique fields to fill out. If the **Cardiology** option is selected, the following fields for entering information additionally appear:
   - normal pressure
   - body mass index
   - Past diseases of the cardiovascular system
   - age
- If the option **Dentist** is selected, additionally it is necessary to fill in:
   - date of last visit
- If the Therapist option is selected, you must additionally fill out:
   - age
- Button `Create`. When a button is clicked, an AJAX request is sent to the corresponding address, and if information about a newly created card is received in the response, a card is created in the Visits Board on the page, and the modal window closes.
- Button `Close` - closes the modal window without saving information and creating a card. By clicking on an area outside the modal window, the modal window is also closed.
- All input fields, regardless of the selected option, except for the field for additional comments, are required for data entry. Data validation is not required.

##### Card describing the visit

The card, which is created on click, appears on the task board. It should look something like this:

![interface](./img/2.png)
   
It must contain:
  - Full name that was entered when creating the card
  - The doctor to whom the person is scheduled for an appointment
  - Button `Show more`. By clicking on it, the card expands, and the rest of the information that was entered when creating the visit appears
  - Button `Edit`. When you click on it, instead of the text content of the card, a form appears where you can edit the entered fields. The same as in the modal window when creating a card
  - Icon with a cross in the upper right corner, when clicked, the card will be deleted

##### Visit filters

Card filter (input field for entering search text by visit title or description, drop-down list by status, drop-down list by priority) you need to do on the front-end - that is, when changing the `value` of any form element (an item in the drop-down list is selected, something was entered in `input`), you filter the list of cards previously received from the server, and display the new information on the screen.

According to the principle of operation, the system should be similar to filters in online stores (for example, on the left [here](https://rozetka.com.ua/notebooks/c80004/)).

##### Classes

JavaScript code must contain the following classes:
  - class Modal (pop-up window);
  - class Visit (describing fields and methods common to all visits to any doctor);
  - child classes VisitDentist, VisitCardiologist, VisitTherapist;

You need to think over the methods and properties of each class yourself. If necessary, you can add other classes as well.

#### Implementation requirements

The design can be anything, but it should be.

#### AJAX part

All the necessary documentation for interacting with the AJAX server can be found [here](https://ajax.test-danit.com/api-pages/cards.html).

#### Optional advanced task

  - When creating a visit card, validate the correctness of the entered data. You can come up with rules for validation yourself (for example, normal pressure should be a number, and be in the range from 50 to 160)
  - Add the ability for the user to move cards around the board using the Drag&Drop method. Such manipulations with the card do not affect the location of other cards. After dragging the card, you do not need to "remember" its new location. When you reload the page, it may return to its original location.