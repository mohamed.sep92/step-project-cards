# step-project-cards


## Description
Create a page that the Secretary can create cards describing scheduled doctor visits. With the help of the application, you can create, edit and delete patient visit records. The ability to sort records by the necessary options (urgency, purpose of visit, etc.) has been implemented. Access to data is possible after authorization on the page using an e-mail and a password. If the login is correct, the necessary information is downloaded from the remote storage.

## Author
This project was created by DANit student as part of the completion of the "Advanced JS" course [Mohamed](https://gitlab.com/mohamed.sep92/step-project-cards/-/tree/master/).

## Gitlab Website

[visit our gitlab page](https://mohamed.sep92.gitlab.io/step-project-cards/)

**Technologies used**
- Javascript
- AJAX
- ES6 modules
- Drag&Drop method

**Resources**
- Bootstrap
- Dragula

## For testing, please login 🔑:
Login: mo@email.com

Password: 12345
