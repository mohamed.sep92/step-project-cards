export default class EmptyInput extends Error{
    constructor(message) {
        super(message);
        this.name = 'Error, Empty Input.'
    }
}